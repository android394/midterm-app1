package com.example.midtermapp1

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.view.View
import android.widget.TextView
import com.example.midtermapp1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val helloName: TextView = findViewById(R.id.textView)
        val helloID: TextView = findViewById(R.id.textView2)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.HelloButton.setOnClickListener{
            Log.d(TAG, ""+helloName.text)
            Log.d(TAG, ""+helloID.text)
            val message = helloName.text
            val intent = Intent(this, HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, message)
            }
            startActivity(intent)
            //setContentView(R.layout.activity_hello)
        }

    }


    companion object {
        private const val TAG = "MainActivity"
    }
}